﻿using System.Collections.Generic;

namespace Contract
{
    public interface IConverter
    {
        IList<string> InputExtensions { get; }
        IList<string> OutputExtensions { get; }
        byte[] Convert(byte[] data, string importFormat, string exportFormat);
    }
}

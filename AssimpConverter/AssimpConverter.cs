﻿using Contract;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using Assimp;
using System.Linq;
using System.IO;
using System.Text;
using System.Runtime.InteropServices;
using System;

namespace AssimpConvert
{
    [Export(typeof(IConverter))]
    public class AssimpConverter : IConverter
    {
        AssimpContext context = new AssimpContext();


        public IList<string> InputExtensions
        {
            get
            {
                return context.GetSupportedImportFormats();
            }
        }

        public IList<string> OutputExtensions
        {
            get
            {
                return context.GetSupportedExportFormats().Select(d => d.FileExtension).ToList();
            }
        }

        [DllImport("user32", CharSet = CharSet.Auto, SetLastError = true)]
        internal static extern int GetWindowText(IntPtr hWnd, [Out, MarshalAs(UnmanagedType.LPTStr)] StringBuilder lpString, int nMaxCount);


        public byte[] Convert(byte[] data, string inputExtension, string exportFormat)
        {
            using (Stream stream = new MemoryStream(data))
            {
                Scene scene = context.ImportFileFromStream(stream, inputExtension);

                ExportFormatDescription exportDescriptor = context.GetSupportedExportFormats().Where(d => d.FileExtension == exportFormat).First();

                ExportDataBlob blob = context.ExportToBlob(scene, exportDescriptor.FormatId);

                return blob.Data;
            }
        }
    }
}

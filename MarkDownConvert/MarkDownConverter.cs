﻿using Contract;
using Markdig;
using System.ComponentModel.Composition;
using System.Text;
using System.Collections.Generic;

namespace MarkDownConvert
{
    [Export(typeof(IConverter))]
    public class MarkDownConverter : IConverter
    {
        IList<string> IConverter.InputExtensions => new List<string>() { ".md" };

        IList<string> IConverter.OutputExtensions => new List<string> { ".html" };

        public byte[] Convert(byte[] data, string importFormat, string exportFormat)
        {
            string stringToParse = Encoding.Default.GetString(data);
            string parsedString = Markdown.ToHtml(stringToParse);

            return Encoding.ASCII.GetBytes(parsedString);
        }
    }
}

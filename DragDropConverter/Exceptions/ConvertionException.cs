﻿using System;

namespace DragDropConverter
{
    class ConvertionException : Exception { }

    class ConverterNotFoundException : Exception { }
}

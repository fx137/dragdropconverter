﻿using Caliburn.Micro;
using Contract;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Windows;

namespace DragDropConverter.ViewModels
{
    class MainViewModel : PropertyChangedBase
    {
        FileHandler fh = new FileHandler();

        public string FileName
        {
            get
            {
                return currentFile?.Name ?? "";
            }
        }
        private FileInfo currentFile;

        public ObservableCollection<KeyValuePair<string, IConverter>> Converters
        {
            get
            {
                var converters = new ObservableCollection<KeyValuePair<string, IConverter>>();
                IEnumerable<IConverter> supportedConverters = fh.GetSupportedConvertions(currentFile?.Extension ?? "");

                foreach (IConverter converter in supportedConverters)
                {
                    foreach (string extension in converter.OutputExtensions)
                    {
                        converters.Add(new KeyValuePair<string, IConverter>(extension, converter));
                    }
                }

                return converters;
            }
        }

        public KeyValuePair<string, IConverter> SelectedConverter { get; set; }

        public MainViewModel()
        {
            fh.ImportPlugins();
        }

        public void Convert()
        {
            try
            {
                fh.ConvertFile(currentFile.FullName, SelectedConverter.Value, SelectedConverter.Key);
            }
            catch (ConverterNotFoundException)
            {
            }
            catch (ConvertionException)
            {
            }
        }

        public void PreviewDragOver(object sender, DragEventArgs e)
        {
            var filePaths = (string[])e.Data.GetData(DataFormats.FileDrop);

            if (filePaths.Length > 1)
            {
                return;
            }
            currentFile = new FileInfo(filePaths[0]);
            e.Handled = true;
            NotifyOfPropertyChange(() => Converters);
            NotifyOfPropertyChange(() => FileName);
        }
    }
}

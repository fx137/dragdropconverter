﻿using Caliburn.Micro;
using DragDropConverter.ViewModels;
using System.Windows;

namespace DragDropConverter
{
    class MainBootstrapper : BootstrapperBase
    {
        public MainBootstrapper()
        {
            Initialize();
        }

        protected override void OnStartup(object sender, StartupEventArgs e)
        {
            DisplayRootViewFor<MainViewModel>();
        }
    }
}

﻿using Contract;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.Diagnostics;
using System.IO;
using System.Linq;

namespace DragDropConverter
{
    class FileHandler
    {
#pragma warning disable 0649
        [ImportMany(typeof(IConverter))]
        private IEnumerable<IConverter> converters;
#pragma warning restore 0649

        public void ImportPlugins()
        {
            var catalog = new AggregateCatalog();
            string assemblyPath = AppDomain.CurrentDomain.BaseDirectory;
            catalog.Catalogs.Add(new DirectoryCatalog(AppDomain.CurrentDomain.BaseDirectory));
            var container = new CompositionContainer(catalog);
            container.ComposeParts(this);
        }

        public IEnumerable<IConverter> GetSupportedConvertions(string extension)
        {
            return converters.Where(d => d.InputExtensions.Select(e => e.ToLower()).Contains(extension.ToLower()));
        }

        public void ConvertFile(string path, IConverter converter, string outExtension)
        {
            if (converter == null)
            {
                throw new ConverterNotFoundException();
            }

            try
            {
                string inExtension = new FileInfo(path).Extension;

                byte[] inData = File.ReadAllBytes(path);
                byte[] outData = converter.Convert(inData, inExtension, outExtension);

                string containingFolder = Path.GetDirectoryName(path);
                string fileName = Path.GetFileNameWithoutExtension(path);
                string newFullFilePath = Path.Combine(containingFolder, fileName + "." + outExtension);

                File.WriteAllBytes(newFullFilePath, outData);
            }
            catch (Exception ex) when (
                ex is IOException
                || ex is UnauthorizedAccessException
                || ex is ArgumentException)
            {
                throw new ConvertionException();
            }
        }
    }
}
